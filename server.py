import socket
import argparse
import random

def simulate_packet_loss(loss_rate):
    return random.uniform(0.0, 1.0) >= loss_rate

def main():
    parser = argparse.ArgumentParser(description='UDP server with optional loss rate')
    parser.add_argument('port', type=int, help='UDP server port')
    parser.add_argument('chunk_size', type=int, help='Chunk size')
    parser.add_argument('loss_rate', type=float, default=0.5, help='Loss rate as a decimal (default: 0.5)')
    args = parser.parse_args()

    port = args.port
    chunk_size = args.chunk_size
    loss_rate = args.loss_rate

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', port))

    print(f"Server is listening on port {port} with chunk size {chunk_size} and loss rate {loss_rate}")

    received_data = b""  # Store received data
    expected_seq_no = 0

    while True:
        data, client_address = server_socket.recvfrom(8192)  # Increased buffer size to accommodate header
        seq_no = int.from_bytes(data[4:8], byteorder='big')
        if simulate_packet_loss(loss_rate):
            print(f"Packet loss, sequence number {seq_no}, bytes from {client_address}")
            continue
        if seq_no == expected_seq_no:
            server_socket.sendto(seq_no.to_bytes(4, byteorder='big'), client_address)  # Acknowledge the received packet
            print(f"Received {len(data)} bytes from {client_address}")
            received_data += data[12:]  # Add received data to the buffer
            expected_seq_no += 1
            print("Result:", received_data.decode())  # Decode received_data and print
        else:
            print(f"Out-of-order packet, sequence number {seq_no}, bytes from {client_address}")

if __name__ == "__main__":
    main()
