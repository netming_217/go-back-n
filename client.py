import socket
import time
import random
import string
import argparse

class GBNStruct:
    def __init__(self, seq_no, ack_no, data_size, data):
        self.seq_no = seq_no
        self.ack_no = ack_no
        self.data_size = data_size
        self.data = data
        self.is_acknowledged = False
        self.last_sent_time = None

    def serialize(self):
        seq_no_bytes = self.seq_no.to_bytes(4, byteorder='big')
        ack_no_bytes = self.ack_no.to_bytes(4, byteorder='big')
        data_size_bytes = self.data_size.to_bytes(4, byteorder='big')
        serialized_data = seq_no_bytes + ack_no_bytes + data_size_bytes + self.data
        return serialized_data

    @staticmethod
    def deserialize(data):
        seq_no = int.from_bytes(data[0:4], byteorder='big')
        ack_no = int.from_bytes(data[4:8], byteorder='big')
        data_size = int.from_bytes(data[8:12], byteorder='big')
        packet_data = data[12:]
        return GBNStruct(seq_no, ack_no, data_size, packet_data)

def parse_arguments():
    parser = argparse.ArgumentParser(description='GBN Client')
    parser.add_argument('server_ip', type=str, help='Server IP address')
    parser.add_argument('server_port', type=int, help='Server port number')
    parser.add_argument('chunk_size', type=int, help='Chunk size')
    parser.add_argument('window_size', type=int, help='Window size')
    return parser.parse_args()

def main():
    args = parse_arguments()
    server_ip = args.server_ip
    server_port = args.server_port
    chunk_size = args.chunk_size
    window_size = args.window_size
    timeout = 0.5
    max_retries = 10

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client_socket.settimeout(timeout)

    text =  "LazyBunnies Wow Wow Wow."
    packets = [GBNStruct(seq_no, seq_no, len(data), data.encode()) for seq_no, data in enumerate(text[i:i+chunk_size] for i in range(0, len(text), chunk_size))]

    print(f"Total Packet: {len(packets)}\n")

    base = 0
    while base < len(packets):
        print(f"Sending packet {packets[base].seq_no}")
        client_socket.sendto(packets[base].serialize(), (server_ip, server_port))
        try:
            data, server_address = client_socket.recvfrom(1024)
            ack_packet = GBNStruct.deserialize(data)
            if ack_packet and ack_packet.ack_no >= base:
                print(f"Received ACK for packet {ack_packet.ack_no} {ack_packet.data.decode()}")
                packets[ack_packet.ack_no].is_acknowledged = True
                base = ack_packet.ack_no + 1
        except socket.timeout:
            print(f"Trys more {max_retries}")
            max_retries -= 1
            if max_retries == 0:
                print("No Response: Timeout")
                return

    print(f"\nFile transfer completed in {len(packets) * timeout:.2f} seconds.")
    throughput = len(packets) / (len(packets) * timeout) if len(packets) * timeout != 0 else 0
    print(f"Throughput: {throughput:.2f} packets per second")
    client_socket.close()

if __name__ == "__main__":
    main()